# PME-CpHMD parameterization and validation

## Thermodynamic integration (TI)

### 1. Prepare Amber topology and parameter files for a model system (ACE-AAXAA-NH2)

* For CHARMM FF, prepare CHARMM topology and parameters first and then use ParmEd's `charmer` to convert them into Amber compatible format.
  
* For Amber FF, use the tools in [cphmd-prep](git@gitlab.com:shenlab-amber-cphmd/cphmd-prep.git) to prepare `rst7` and `parm7` files. Examples of these files can be found in validation directory. HMR inputs (Hydrogen Mass Repartitioning) are also present there, with HMR inputs you can alter time step from 0.002 ps to 0.004 ps in the production stage. 
  
* For either FF, make sure the files are named properly. For example, if we want to parameterize Cys, we should name them `cys.rst7` and `cys.parm7` so that other scripts can recognize them

### 2. Minimization

* Run minization script:
`min.src cys`

### 3. TI simulations to determine potential of mean force (TI) for titrating the model

* Run simulations with fixed θ values using the script `heat_equil_prod_param.sh` in the TI folder; remember to copy the `charmm_pme.parm` and `prod.phmdin` files to the working directory as well.

`bash heat_equil_prod_param.sh cys`

* Such script will heat and equilibrate your system. After that, it will perform the production of 10 ns of simulations (default). 


### 4. Fitting of the mean forces to obtain the parameters in the model PMF

* In the output prod*.lambda contains dU/dθ values as a function of simulation steps for each fixed λ simulated.

* Run the script `cphmd_parm_fit.py`. 

`cphmd_parm_fit.py [label (default: prod)] [fit_type (default: single)] [begin (default: 0)] [end (default: -1)])`

* `cphmd_parm_fit.py` generates a file du.data with <dU/dθ> as a function of θ. Also, the script fittes this function using the expression <dU/dθ> = 2A sin2(θ) sin(2θ) - B. From this fit is obtained the values of parameters A and B.

### Validation

* Add parameters A and B in file: `charmm_pme.parm -> PARAMETERS(5,:) = A,B,`

* Run independent pH simulations using [`heat_equil_prod_protein.sh`](https://gitlab.com/shenlab-amber-cphmd/amber-ephmd/parameterization/-/blob/master/validation/c22ff_protocol/heat_equil_prod_protein.sh) in the `validation/c22ff_protocol` folder.

* Check the convergence of λ in each pH simulated and the pKa, compare the pKa value with the compound model (for Cys pKa = 8.5). The analysis can be made using the tools in [cphmd-analysis](https://gitlab.com/shenlab-amber-cphmd/cphmd-analysis) 
