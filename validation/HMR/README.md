# Generation of topology files with Hydrogen mass repartitioning (HMR)

HMR redistributes some of the mass from heavy atoms connected to hydrogens to the bonded hydrogens, which allows a larger integration timestep without causing instability due to the high-frequency motino of hydrogens. For example, to run simulations with a 4 fs time step instead of the standard 2 fs time step you can use parmed to perform HMR on an existing parm7 file:

### ParmEd example:
`parmed` (start parmEd)

`parm name.parm7` (load topology file)

`hmassrepartition` (Change hydrogen masses to 3.024 daltons, decrease in the same amount the masses of heavy atoms connected to the hydrogens. The entire mass of the solute is kept. Not changing water hydrogen masses.)

`outparm name_hmass.parm7` (Generate HMR topology file)

`quit` (Finish parmEd)

Obs: If you need a HMR topology file for CHARMM (.psf), you can also used parmEd to convert **name_hmass.parm7** to a **name_hmass.psf**

You can find a detailed tutorial and references here: [HMR](https://ambermd.org/tutorials/basic/tutorial12/index.php) and [amber 220 manual](https://ambermd.org/doc12/Amber20.pdf#page=271), pag. 271.
